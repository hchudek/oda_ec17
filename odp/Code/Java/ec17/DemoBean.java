package ec17;


import java.io.Serializable;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.Item;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class DemoBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2169909770379641711L;
	private Session session = null;
	private Database db = null;
	
	//Beans benötigen einen leeren Konstruktor,
	//Hier kann auch z.B. die Session-Initilisierung gemacht werden
	public DemoBean(){
		// Server-Sitzung
		session = Factory.getSession(SessionType.NATIVE);
		//Aktuelle Benutzer-Sitzung
		//session = Factory.getSession(SessionType.CURRENT);
		
		//Aktuelle Datenbank holen
		this.db = session.getCurrentDatabase();
		
		//Beliebige Datenbank holen
		//db= session.getDatabase("ReplicaID oder Pfad oder Server!!Pfad");
	}
	
	//Funktions-Platzhalter für leere Funktion
	public void emptyStub(){
		
	}
	
	//Funktions-Platzhalter für Funktion mit bool-Rückgabe
	public boolean boolStub(){
		return true;
	}
	
	//Platzhalter für Dokumenten-Aktionen
	public void runActionDocument(){
		
	}
	
	//Platzhalter für Ansichten-Funktionen 
	public void runActionView(){
		
	}
	
	//Platzhalter für Datenbank-Funktionen
	public void runActionDb(){
		
	}
	
	//Platzhalter für Transaktionen
	public void runActionTransactions(){
		
	}
	
	//Platzhalter für Xots
	public void runActionXots(){
		
	}
	
	//Platzhalter für Datum / Zeit
	public void runActionDateTime(){
		
	}
	
}
